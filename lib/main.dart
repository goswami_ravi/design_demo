import 'package:design_demo/constants.dart';
import 'package:design_demo/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
        ),
        debugShowCheckedModeBanner: false,
        home: ContactList());
  }
}

class ContactList extends StatefulWidget {
  @override
  _ContactListState createState() => _ContactListState();
}

class _ContactListState extends State<ContactList> {
  int selectedSegment = 0;
  @override
  Widget build(BuildContext context) {
    final Map<int, Widget> children1234 = const <int, Widget>{
      0: Text('ALL'),
      1: Text('  Favourite  '),
    };

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Profile()),
          );
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          children: <Widget>[
            Container(
              height: 44,
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                ),
              ),
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 12),
                  CupertinoSegmentedControl(
                    children: children1234,
                    onValueChanged: (int value) {
                      setState(() {
                        selectedSegment = value;
                      });
                    },
                    groupValue: selectedSegment,
                  ),
                  SizedBox(height: 6),
                  Expanded(
                    child: ListView.builder(
                      itemCount: Contact.list.length * 4,
                      padding: EdgeInsets.only(
                          left: 16.0,
                          right: 16.0,
                          bottom: MediaQuery.of(context).padding.bottom),
                      itemBuilder: (BuildContext context, int index) {
                        return new ContactCell(
                            contact: Contact.list[index % 7]);
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ContactCell extends StatefulWidget {
  final Contact contact;

  const ContactCell({Key key, this.contact}) : super(key: key);

  @override
  _ContactCellState createState() => _ContactCellState();
}

class _ContactCellState extends State<ContactCell> {
  @override
  Widget build(BuildContext context) {
    return Slidable(
      child: Container(
        child: Column(
          children: <Widget>[
            Divider(
              height: 1,
            ),
            Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Image.asset(
                    widget.contact.image,
                    height: 40,
                    width: 40,
                  ),
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 8),
                    Text(widget.contact.name,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24)),
                    Text(widget.contact.proffession, textAlign: TextAlign.left),
                    SizedBox(height: 8)
                  ],
                ),
                Spacer(),
                Icon(Icons.star)
              ],
            ),
          ],
        ),
      ),
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.2,
      secondaryActions: <Widget>[
        IconSlideAction(
          color: Theme.of(context).primaryColor,
          caption: 'Edit',
          icon: Icons.edit,
        ),
        IconSlideAction(
          color: Colors.red,
          caption: 'Delete',
          icon: Icons.delete,
        ),
      ],
      closeOnScroll: true,
    );
  }
}
