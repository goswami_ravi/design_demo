import 'package:flutter/material.dart';

class AppImage {
  static String iconBlog = 'assets/account.png';
  static String user1 = 'assets/dummy1.jpg';
  static String user2 = 'assets/dummy2.jpg';
  static String user3 = 'assets/dummy3.jpg';
  static String user4 = 'assets/dummy4.jpg';
  static String user5 = 'assets/dummy5.jpg';
  static String user6 = 'assets/dummy6.jpg';
  static String user7 = 'assets/dummy7.jpg';

  static List<String> userList = [
    user1,
    user2,
    user3,
    user4,
    user5,
    user6,
    user7
  ];
}

class Contact {
  final String name;
  final String proffession;
  final String image;

  Contact({this.name, this.proffession, this.image});

  static List<Contact> list = [
    Contact(
      name: 'John Snow',
      proffession: 'Lord Commander of the Wall',
      image: AppImage.user1,
    ),
    Contact(
      name: 'Hannible Lecter',
      proffession: 'Psychiatrist',
      image: AppImage.user2,
    ),
    Contact(
      name: 'Voldemort',
      proffession: 'Wizard',
      image: AppImage.user3,
    ),
    Contact(
      name: 'John Wick',
      proffession: 'Boogyman',
      image: AppImage.user4,
    ),
    Contact(
      name: 'Saitama',
      proffession: 'Hero for fun',
      image: AppImage.user5,
    ),
    Contact(
      name: 'Monkey D. Luffy',
      proffession: 'King of Pirates',
      image: AppImage.user6,
    ),
    Contact(
      name: 'GOKU',
      proffession: 'Legendary Super Saiyen',
      image: AppImage.user7,
    ),
  ];
}
