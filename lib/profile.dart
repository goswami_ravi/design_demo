import 'package:flutter/material.dart';

import 'constants.dart';
import 'edit_profile.dart';

class Profile extends StatelessWidget {
  const Profile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(height: MediaQuery.of(context).padding.top + 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 60),
              Spacer(),
              Container(
                child: Hero(
                  child: Image.asset(
                    AppImage.iconBlog,
                    height: 200,
                    width: 200,
                  ),
                  tag: 'image',
                ),
              ),
              Spacer(),
              InkWell(
                child: Icon(Icons.edit),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => EditProfile()),
                  );
                },
              ),
              SizedBox(width: 10)
            ],
          ),
          SizedBox(height: 28),
          Text(
            "John Smith",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 34),
          ),
          Text("frontend Developer",
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18)),
          SizedBox(height: 8),
          Text("Binary",
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16)),
          SizedBox(height: 28),
          Divider(indent: 20, height: 6, color: Colors.black, endIndent: 20),
          SizedBox(height: 12),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Mobile",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              SizedBox(width: 20),
              Text(
                "+12 345 678 901",
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 20),
              )
            ],
          ),
          SizedBox(height: 18),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Icon(Icons.phone, size: 30),
              SizedBox(width: 20),
              Icon(Icons.message, size: 30),
              SizedBox(
                width: 80,
              )
            ],
          ),
          SizedBox(height: 18),
          Divider(indent: 20, height: 6, color: Colors.black, endIndent: 20),
        ],
      ),
    );
  }
}
