import 'package:flutter/material.dart';

import 'constants.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Contact"),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        children: <Widget>[
          SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Hero(
                  child: Image.asset(
                    AppImage.iconBlog,
                    height: 200,
                    width: 200,
                  ),
                  tag: 'image',
                ),
              ),
            ],
          ),
          SizedBox(height: 28),
          Divider(height: 6, color: Colors.black),
          SizedBox(height: 12),
          DataField(
            label: 'Name',
            text: 'John Cena',
          ),
          SizedBox(height: 12),
          DataField(
            label: 'Title',
            text: 'Fighter',
          ),
          SizedBox(height: 12),
          DataField(
            label: 'Company',
            text: 'WWE',
          ),
          SizedBox(height: 18),
          Divider(height: 6, color: Colors.black),
          SizedBox(height: 18),
          DataField(
            label: 'Mobile',
            text: '+1 123 234 34',
          ),
          SizedBox(height: 32),
          RaisedButton(
            padding: EdgeInsets.symmetric(vertical: 12),
            color: Theme.of(context).primaryColor,
            child: Text(
              'Save',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          )
        ],
      ),
    );
  }
}

class DataField extends StatelessWidget {
  final String label;
  final String text;

  const DataField({this.label, this.text});
  @override
  Widget build(BuildContext context) {
    TextEditingController _controller = TextEditingController(text: text);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          width: 100.0,
          child: Text(
            label,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
          ),
        ),
        SizedBox(width: 20),
        Expanded(
          child: TextField(
            controller: _controller,
            decoration: new InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.greenAccent, width: 1.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black38, width: 1.0),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
